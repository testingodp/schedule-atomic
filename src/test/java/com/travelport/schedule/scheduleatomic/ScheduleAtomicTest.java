package com.travelport.schedule.scheduleatomic;

import java.io.File;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightScheduleRQ;

@RunWith(JUnit4.class)
public class ScheduleAtomicTest {

  @Test
  public void testdoCreate_v0() throws Exception {
    JAXBContext jaxbContext = JAXBContext.newInstance(FlightScheduleRQ.class);
    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    final File file = new File("src/test/resources/mockData/FlightSpecificRequest.xml");
    FlightScheduleRQ flightScheduleRQ = (FlightScheduleRQ) jaxbUnmarshaller.unmarshal(file);
    ScheduleAtomic atomic = new ScheduleAtomic();
    atomic.doCreate_v0(flightScheduleRQ, "0");
  }

}
