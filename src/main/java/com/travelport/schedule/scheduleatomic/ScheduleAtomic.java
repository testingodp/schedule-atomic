package com.travelport.schedule.scheduleatomic;

import javax.ws.rs.Path;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.travelport.odt.restfw.plugin.common.annotations.OTMResource;
import com.travelport.odt.restfw.plugin.common.exceptions.ServiceException;
import com.travelport.schedule.fsschedule.atomic.impl.FlightSpecificScheduleUtil;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.AbstractFlightScheduleResource;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightScheduleRQ;
import com.travelport.training.od_bootcamp.tcs.mp3.resource.FlightScheduleRS;

@Path("AIRSCHEDULES/FlightSchedules")
@OTMResource(systemId = "AIRSCHEDULES", namespace = "http://training.travelport.com/od_bootcamp/tcs/mp3/resource/v0", localName = "FlightScheduleResource")
public class ScheduleAtomic extends AbstractFlightScheduleResource {

	
	public static final Logger LOGGER = LoggerFactory.getLogger(ScheduleAtomic.class);

	@Override
	protected FlightScheduleRS doCreate_v0(FlightScheduleRQ request, String version) throws ServiceException {
		FlightSpecificScheduleUtil resource = new FlightSpecificScheduleUtil();
		return resource.retrieveAirSchedule(request, "123", false);
	}

	
	
	

	
	
	  
	  

}
